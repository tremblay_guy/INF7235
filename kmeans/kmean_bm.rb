require 'benchmark'
require_relative 'kmean'
require_relative 'kmean_static'
require_relative 'kmean_static_fj'
require_relative 'kmean_dynamic'
require_relative 'dataset'

#Number of point per sets
nbData = [
    100,
    500,
    4000,
    6000,
]
dataList = nbData.map { |n| gen(n) }

#Number of centroids
nbCentroids = [
    1,
    2,
    20
]

#Number of call done per set dataList x nbCentroids
#Average result of nbTry
nbTry = 1

#Temps moyen
#Centroids, Seq, Kmeans1, Kmeans2, RelKmeans1, RelKmeans2
tab = "%-8s %-8s %-8s %-13s %-13s %-13s\n"
printf tab, "Centroids", "Data size", "Sequential", "Static", "Static FJ", "Dynamic"
nbCentroids.each do |i|
    printf tab, i, "", "", "", "", "", "", ""
    dataList.each_with_index do |data, index|
        result = [0,0,0,0]
        time = 0
        (0...nbTry).each do |j|
            time += Benchmark.measure{
                p = Kmeans.new(i, 0.1, 500)
                p.work(data)
            }.real
        end
        result[0] = (time/nbTry).round(5)

        time = 0
        (0...nbTry).each do |j|
            time += Benchmark.measure{
                p = KmeansStatic.new(i, 0.1, 500)
                p.work(data)
            }.real
        end
        result[1] = (time/nbTry).round(5)

        time = 0
        (0...nbTry).each do |j|
            time += Benchmark.measure{
                p = KmeansStaticFJ.new(i, 0.1, 500)
                p.work(data)
            }.real
        end
        result[2] = (time/nbTry).round(5)

        time = 0
        (0...nbTry).each do |j|
            time += Benchmark.measure{
                p = KmeansDynamic.new(i, 0.1, 500)
                p.work(data)
            }.real
        end
        result[3] = (time/nbTry).round(5)

        printf tab, "", nbData[index], result[0], result[1].to_s + " x" + (result[0]/result[1]).round(2).to_s, result[2].to_s + " x" + (result[0]/result[2]).round(2).to_s, result[3].to_s + " x" + (result[0]/result[3]).round(2).to_s
    end
end
