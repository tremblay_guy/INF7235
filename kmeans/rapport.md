#Version séquentielle
kmeans.rb

La version séquentielle est O(nm^2c).

n: nombre d'itérations avant le résultat stable
m: nombre de points
c: nombre de centroïdes

Ceci dit, le nombre d'itération est relativement bas et le nombre de centroïdes encore plus.

À titre d'exemple, considérant que l'optimisation est atteinte (tolérance de 0.1):

nb. Point (m)       Itérations (n)       nb. Centroïdes
4000                25                   6
8000                40                   6
8000                50                   10 
20000               50                   10
40000               50                   10

On peut donc assumer que le problème kmeans est de O(m^2).

## Version optimale
L'algorithme est connu pour être un problème NP lorsque le nombre d'itération est infini.

Il est considéré que la version séquentielle présentée ici est optimale considérant un nombre d'itération fini. Pour que l'algorithme kmeans fonctionne chaque point de l'ensemble doit être analysé par chacun des clusters et ensuite chacun de ces points doit être repassés selon chaque cluster. On ne peut donc pas séparer cet ensemble de point selon une stratégie "Diviser pour régner" par exemple.

#PCAM, métriques et réflexions
On peut déjà d'emblée apercevoir que l'algorithme K-Means est potentiellement un problème de parallélisme "semi-embarrassant". Certes, les centroïdes se partagent l'ensemble des points, mais ceux-ci font chacun un travail indépendant et identique aux autres. 

## Partionnement
Les étapes obligatoirement séquentielles sont indiquées.

Les opérations du programme sont les suivantes:
- Initialisation des centroids (séquentiel) 
- Initialisation des grappes vides (séquentiel)
- Itération sur l'ensemble des points
    - Calcul de la distance du point avec chaque centroïde
    - Sélection de la grappe la plus proche pour ce point
- Calcul de la distance moyenne selon l'ensemble des points de la grappe
    - Moyenne des x
    - Moyenne des y
- Évaluation de la tolérance de chaque centroïde selon le changement par rapport à l'ancienne version de celui-ci 

Le patron algorithme qui resort est celui de "Parallélisme de données". K-Means évalue une grande quantité de données, les tâches sont relativement simples (le calcul de la distance) et les opération se répètement uniformément pour l'ensemble des données. D'ailleurs, la quantité de parallélisme augmente proportionnellement avec la quantité de données. Plus précisément, il s'agit de parallélisme de résultat. À preuve, il est indiqué, avant de lancer l'algorithme, le nombre (le résultat) de centroïdes désirés et ce sera potentiellement par les grappes de chaque centroïde que le problème pourra se paralléliser.

## Communication
Séquentiellement, chaque tâche mentionné dans le partionnement dépend de celle qui la précède, sauf à quelques exceptions près comme le calcul des moyennes.

## Agglomération
Il y a deux sections de l'algorithme qui peuvent se regrouper afin de créer une granularité plus grossière: l'itération sur l'ensemble des points et le calcul de la distance moyenne par grappe

## Mapping
L'approche PCAM exprime qu'il faut associer les tâches à des threads. Cependant, dans le cas actuel, considérant qu'il s'agit de parallélisme de résultat, les threads seront associés, statiquement, à chaque centroïde. Le défaut de cette approche est le problème "scale" en fonction du nombre de centroïdes et non en fonction de la quantité de données. Il sera donc pertinent de concevoir une approche dynamique en fonction des points plutôt que des centroïdes, où un mélange des deux.

# Métrique
Les métriques ont été calculé en fonction de trois critères
1. Le problème est optimale 
2. Le nombre de centroïde
3. Le nombre de point

Note des résultats :
- Le parallélisme de données n'est pas concluant, la quantité de centroides est trop faible à elle seule pour offrir des gains. Rien de très notable, les gains sont minimes à partir de 30000 et 50000 points.

- L'approche avec le peach_index sur le data est extrêmement bénéfique. Le problème de performance se situe à l'itération. Bref, il ne s'agit pas du mouvement ni du calcul du ratio mais du fait d'itérer jusqu'à une optimisation. Le retour à la boucle semble ralentir le parallélisme puisque les 64 threads sont constamment recréés. Il faudrait donc les conserver. Worker pool?
