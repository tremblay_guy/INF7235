require_relative 'dataset'

begin
    #If graphical output is wanted
    require 'gnuplot'
    require 'pruby'
rescue LoadError => e
    puts e
end

class Point
    def initialize(x, y)
        @x = x.to_f
        @y = y.to_f
    end

    #Ruby getters (GT) => attr_reader
    attr_reader :x, :y

    #Distances
    def euclidean_distance(pointB)
        Math.sqrt( (@x - pointB.x) ** 2 + (@y - pointB.y) ** 2)
    end

    #utils (GT): Pas besoin de return
    def to_arr
        [@x, @y]
    end

    def to_s
        to_arr.to_s
    end

    def ==(other)
        return nil unless other.kind_of?(Point)

        x == other.x && y == other.y
    end
end

class Kmeans
    def initialize(kCount = 2, tolerance = 0.1, iteration = 20)
        @tolerance = tolerance
        @iteration = iteration
        @kCount = kCount
    end

    attr_reader :centroids, :clusters

    def work(data)
        if data.size < @kCount
            puts "Too much centroids for data"
            exit 1
        end

        @centroids = Hash.new

        (0...@kCount).each do |i|
            #base initial centroids points on first data points
            #centrois are Points instances.
            @centroids[i] = data[i]
        end

        # (GT) On evite d'utiliser for en Ruby. On utilise plutot each.
        #Start iteration
        (0..@iteration).each do |i|
            #New clusters (centroids has moved)
            #This classification is renewed until the tolerance is good enough
            @clusters = Hash.new
            #Prepare empty clusters of data
            (0...@kCount).each do |k|
                @clusters[k] = []
            end

            #Associate each point with its closest cluster
            data.each do |point|
                #Get distances for each cluster
                #Note that clusters count is the same as the centroids one, sharing the
                #mapperId concept
                distances = Hash.new

                # (GT) Pour les blocs, on utilise les accolades uniquement si ca fitte
                # sur une ligne. Sinon, on utilise do/end
                @centroids.each do |mapperId, centroidPoint|
                    distances[mapperId] = centroidPoint.euclidean_distance(point)
                end

                #Associate lowest distance with proper cluster
                #by finding minimum distance for that point
                minDistance = nil # (GT): Non, pas de constante magique!!
                chosenId = nil
                distances.each do |mapperId, distanceValue|
                    if minDistance.nil? || distanceValue < minDistance
                        minDistance = distanceValue
                        chosenId = mapperId
                    end
                end
                @clusters[chosenId].push(point)
            end

            #Prepare centroids comparison, checking if the new centroids respect tolerance
            oldCentroids = centroids.clone # (GT): Pas de () si aucun argument.

            #moving centroids
            @clusters.each do | mapperId, pointList |
                centroids[mapperId] = average_point(pointList)
            end

            optimized = true

            #Iterate each key with their associated point list, check the ratio by which centroids
            #centroids moved
            @centroids.each do |centroidId,centroidPoint|
                originCentroid = oldCentroids[centroidId]
                if change_ratio(originCentroid, centroidPoint) > @tolerance
                    optimized = false
                    break
                end
            end

            return if optimized  # (GT): Forme/Style Ruby: avec garde!
        end
        puts ":::Could not optimized centroids."
    end

    def average_point(pointList)
        #TODO Passage par référence?
        #Calculate the average position base on a point list
        # (GT): Quand possible, on utilise reduce.
        sumX, sumY = pointList.reduce([0, 0]) do |sum, point|
            [sum.first + point.x, sum.last + point.y]
        end

        Point.new(sumX / pointList.size, sumY / pointList.size)
    end

    def change_ratio(oldPoint, newPoint)
        #Calculate the change proportion between an old point and a new point
        x_ratio = ((newPoint.x-oldPoint.x)/oldPoint.x * 100.0).abs
        y_ratio = ((newPoint.y-oldPoint.y)/oldPoint.y * 100.0).abs

        #Cover for zero division
        x_ratio = x_ratio.nan? ? oldPoint.x : x_ratio
        y_ratio = y_ratio.nan? ? oldPoint.y : y_ratio

        # (GT): Pas besoin de return, sauf si on retourne avant la fin
        #Choose highest ratio to evaluate the change
        x_ratio > y_ratio ? x_ratio : y_ratio
    end

    def output
        format = "%#{}s\t%#{}s\n"
        printf(format, "Centroids", "Point list")
        printf(format, "---------", "----------")
        @centroids.each do |centroidId, centroidPoint|
            pointListStr = ""
            @clusters[centroidId].each do |point|
                # (GT): <<, contrairement a +=, evite d'allouer une nouvelle chaine.
                pointListStr << point.to_s << ", "
            end
            printf(format, centroidPoint, pointListStr)
        end
    end

    def output_centroid()
        format = "%#{}s\n"
        printf(format, "Centroids")
        printf(format, "---------")
        @centroids.each do |centroidId, centroidPoint|
            printf(format, centroidPoint)
        end
    end

    def plot

        color = ["black", "red", "blue", "brown", "pink", "green", "black", "orange"]

        Gnuplot.open do |gp|
            Gnuplot::Plot.new(gp) do |plot|
                @clusters.each do |clusterId,pointList|
                    x = []
                    y = []
                    pointList.each do |point|
                        x << point.x
                        y << point.y
                    end

                    xCentroid = centroids[clusterId].x
                    yCentroid = centroids[clusterId].y

                    chosenColor = color.pop()
                    plot.data << Gnuplot::DataSet.new([x, y]) do |ds|
                        ds.with = 'point lt rgb "' + chosenColor + '"'
                        ds.notitle
                    end
                    plot.data << Gnuplot::DataSet.new([xCentroid, yCentroid]) do |ds|
                        ds.with = 'point lt rgb "' + chosenColor + '"'
                        ds.title = "Centroid (" + chosenColor + ") : " + xCentroid.to_s + ", " + yCentroid.to_s
                    end
                end
            end
        end
    end
end

#p = Kmeans.new(ARGV[0].to_i, 0.1, 500)
#n = ARGV[0].nil? ? 4000 : ARGV[1] #default 4000
#p.work(gen(n.to_i))

# p.output()
# p.output_centroid()
# p.plot()
