#Generate a number of point
def gen(n=4000, range_max=2500)
    seed = Random.new(3456)
    data = Array.new(n) { Point.new(seed.rand(0..range_max), seed.rand(0..range_max))}
    data
end

def simple()
    data = [
        Point.new(1,2),
        Point.new(1.5,1.8),
        Point.new(5,8),
        Point.new(8,8),
        Point.new(1,0.6),
        Point.new(9,11),
    ]
    data
end

    
