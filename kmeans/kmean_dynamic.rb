# -*- coding: iso-8859-1 -*-
require_relative 'dataset'
require 'pruby'

begin
  #If graphical output is wanted
  require 'gnuplot'
rescue LoadError => e
  puts e
end

class Point
  def initialize(x, y)
    @x = x.to_f
    @y = y.to_f
  end

  attr_reader :x, :y

  # Distances
  def euclidean_distance(pointB)
    Math.sqrt( (@x - pointB.x) ** 2 + (@y - pointB.y) ** 2)
  end

  def to_arr
    [@x, @y]
  end

  def to_s
    to_arr.to_s
  end

  def inspect
    to_arr.to_s
  end

  def +(other)
    Point.new(x + other.x, y + other.y)
  end

  def *(scalar)
    Point.new(x * scalar, y * scalar)
  end

  def ==(other)
    return nil unless other.kind_of?(Point)

    x == other.x && y == other.y
  end
end

class KmeansDynamic
  def initialize(kCount = 2, tolerance = 0.1, iteration = 20)
    @tolerance = tolerance
    @iteration = iteration
    @kCount = kCount
  end

  attr_reader :centroids, :clusters

  def work(data)
    if data.size < @kCount
      puts "Too much centroids for data"
      exit 1
    end

    # Base initial centroids points on first data points. Centroids
    # are Points instances.
    @centroids = Array.new(@kCount) { |i| data[i] }

    # Start iteration
    @iteration.times do |i|
      # New clusters (centroids has moved)
      # This classification is renewed until the tolerance is good enough

      @clusters = _build_cluster(data)
      # Prepare centroids comparison, checking if the new centroids respect tolerance
      # Moving centroids
      old_centroids = @centroids.clone
      @centroids = @clusters.map { |cluster| average_point(cluster) }

      # Iterate each key with their associated point list, check the
      # ratio by which centroids centroids moved
      return if @centroids.zip(old_centroids).all? {|old_c, new_c| change_ratio(old_c, new_c) <= @tolerance}
    end
    puts ":::Could not optimized centroids."
  end

  # (GT) Constante magique: 5! Et pourquoi 5 threads?
  def _build_cluster(data)
      # Each index will, by position, know which cluster it belongs to
      # point 0 has cluster X
      # Associate each point with its closest cluster
      @data_array = (0...data.size).pmap(dynamic:true, nb_threads: 5) do |index|
        # Get distances for each cluster
        # Note that clusters count is the same as the centroids one, sharing the
        # mapper_id concept
        distances = @centroids.map { |centroid| centroid.euclidean_distance(data[index]) }
        closest_cluster(distances)
      end

      @clusters = (0...@kCount).map do |c_id|
        points_with_id(@data_array, c_id).map { |index| data[index] }
      end

      @clusters
  end

  def points_with_id(data_array, c_id)
    (0...data_array.size).select { |k| data_array[k] == c_id }
  end

  def closest_cluster(distances)
    # Associate lowest distance with proper cluster
    # by finding minimum distance for that point
    (1...distances.size).reduce([0, distances[0]]) do |min_id_dist, k|
      min_id, min_distance = min_id_dist

      distances[k] < min_distance ? [k, distances[k]] : min_id_dist
    end
      .first
  end

  def average_point(point_list)
    point_list.preduce(Point.new(0.0, 0.0), &:+) * (1.0 / point_list.size)
  end

  def change_ratio(oldPoint, newPoint)
    # Calculate the change proportion between an old point and a new point
    x_ratio = ((newPoint.x - oldPoint.x) / oldPoint.x * 100.0).abs
    y_ratio = ((newPoint.y - oldPoint.y) / oldPoint.y * 100.0).abs

    # Cover for zero division
    x_ratio = x_ratio.nan? ? oldPoint.x : x_ratio
    y_ratio = y_ratio.nan? ? oldPoint.y : y_ratio

    # Choose highest ratio to evaluate the change
    [x_ratio, y_ratio].max
  end

  def output
    format = "%#{}s\t%#{}s\n"
        printf(format, "Centroids", "Point list")
        printf(format, "---------", "----------")
        @centroids.each_index do |centroid_id|
            point_list_str = ""
            @clusters[centroid_id].each do |point|
                point_list_str << point.to_s << ", "
            end
            printf(format, @centroids[centroid_id], point_list_str)
        end
    end

    def output_centroid()
        format = "%#{}s\n"
        printf(format, "Centroids")
        printf(format, "---------")
        @centroids.each_index do |centroid_id|
            printf(format, @centroids[centroid_id])
        end
    end

    def plot
        color = ["black", "red", "blue", "brown", "pink", "green", "black", "orange"]

        Gnuplot.open do |gp|
            Gnuplot::Plot.new(gp) do |plot|
                @clusters.each do |cluster_id,point_list|
                    x = []
                    y = []
                    point_list.each do |point|
                        x << point.x
                        y << point.y
                    end

                    xCentroid = centroids[cluster_id].x
                    yCentroid = centroids[cluster_id].y

                    chosenColor = color.pop()
                    plot.data << Gnuplot::DataSet.new([x, y]) do |ds|
                        ds.with = 'point lt rgb "' + chosenColor + '"'
                        ds.notitle
                    end
                    plot.data << Gnuplot::DataSet.new([xCentroid, yCentroid]) do |ds|
                        ds.with = 'point lt rgb "' + chosenColor + '"'
                        ds.title = "Centroid (" + chosenColor + ") : " + xCentroid.to_s + ", " + yCentroid.to_s
                    end
                end
            end
        end
    end
end

# p = Kmeans.new(8, 0.1, 50)
# n = ARGV[0].nil? ? 4000 : ARGV[0] #default 4000
# p.work(gen(n.to_i))

# p.output()
# p.output_centroid()
# p.plot()
